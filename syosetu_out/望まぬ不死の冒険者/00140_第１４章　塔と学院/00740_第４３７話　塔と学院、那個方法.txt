「嗯？還有什麼事嗎？」

我們停下腳步，回頭望向菲麗絲，奧格利作為代表這樣問道
菲麗絲低著頭，似乎在猶豫什麼，最後終於露出下定決心的表情，抬起頭來向我們開了口
不，大概主要不是向我們，而是向奧格利吧

⋯⋯這樣沒問題麼

「那個！穿過水色飛龍棲息地的方法⋯⋯各位想知道的吧⋯⋯？所以，我，可以把方法教給各位⋯⋯」

這還真是出乎意料啊
我們當然知道，她掌握有這種情報

但考慮到其重要性、危險性，就算她不願意透露，也情有可原
可為何事到如今，又改變了主意呢
雖然對我們來說，正可謂求之不得就是了

不過吶⋯⋯

「菲麗絲，你不用勉強自己，對你來說，那不是很重要的情報嗎？」

奧格利像是在擔心菲麗絲似的說到
若不是像我們這樣彼此心有靈犀的小隊，恐怕就要變成「喂，奧格利，好不容易要打聽到情報了，你多什麼嘴」的情況了吧

但是，我們早就在這一點上達成了共識
也就是說，若菲麗絲不願意吐露情報，就絶不強求
所以奧格利並未先徵求我們的同意，就直接與菲麗絲交談了起來

菲麗絲又低下頭去，小聲說著

「⋯⋯確實如此，不過，奧格利先生⋯⋯救了我的命，而且，雷特先生和羅蕾露小姐也是⋯⋯沒有傷害村裡的其他人。其實，當時的情況，比起無傷捉住大家⋯⋯直接殺死要簡單得多吧？」

她說的沒錯
特別是羅蕾露，若動起真格來，把整個村子化為焦土大概也花不了太長時間
換我出手的話，就會量產被吸乾血液的乾屍了
奧格利的話⋯⋯大概會踏踏實實一個接一個斬殺對手吧，即便如此，也花不了半日功夫
而且還能確實消滅掉後顧之憂，萬萬歳呢

⋯⋯什麼的

但我們是不會做出那種慘無人道之事的，而且，就算真的做了，也不見得就能完全消除後顧之憂

若沒有目擊者在場，只要我們適當編造一番話，就能糊弄過去了，但村裡還潛伏著《哥布林》他們呢
十有八九，之後會讓事情複雜化起來
換句話說，解決事件的同時，注意不傷到村民們，也是為了我們自己著想⋯⋯並不是需要被如此感謝的事情啊

但菲麗絲繼續說道

「所以我想著，要為你們做點什麼當成謝禮，但是，我又不是特別擅長料理⋯⋯可也想不出其它能幫上忙的事情⋯⋯只有通過水色飛龍棲息地的方法⋯⋯」

這就是她唯一能給我們的謝禮麼
真是個好姑娘呢，能感受到她的決意
奧格利似乎也和我想法一致

「如果你是認真的，當然歡迎。而且，我們可以發誓，即使知道了那個方法，也絶不向外人透露。但是，真的可以嗎？」

最後問了一句
菲麗絲點了點頭

「是的！我相信大家！」

這樣說到


◇◆◇◆◇

水色飛龍的棲息地，位於彼得利瑪湖周邊

「⋯⋯真的有幾千匹啊，看到這個還想著正面突破的傢伙，肯定是笨蛋」

我在湖邊森林的草叢裡藏起身影，這樣嘟囔了一句。藏在一旁的羅蕾露小聲說道

「你這不是罵自己麼，雖然我也有同感⋯⋯不過，這裡景色不錯啊，藍色的湖面倒映著水色飛龍的身影，還漂浮著不少浮遊石，很夢幻的感覺呢。據說是因為岩石中含有細微的魔石，才會變成那樣⋯⋯」

彼得利瑪湖上，以及四周的空中，漂浮著一些大小不一的岩石

《新月迷宮》第四層入口處也有類似的岩石

那邊是獨一塊的巨岩，而這裡則有許多小一些的，差不多大小的也有幾塊
在那些岩石上，水色飛龍用不知那裡叼來的樹枝石塊，還有魔物骨頭築成了巢
巢裡應該就是飛龍蛋了吧，但在我們的角度上觀察不到
雖然我伸出背後的翅膀，飛到上空應該就能看到了，但那樣根本無異於衝到飛龍群裡送死
然後被一口吞下⋯⋯還是饒了我吧

水色飛龍會在彼得利瑪湖築巢，除了因為湖泊，浮遊石也是一大原因。其它被確認到的水色飛龍棲息地或繁殖地，也都是差不多的地域

「大概是因為在高處築巢，可以保護卵不受各種外敵的侵害吧。因為漂浮在空中，所以一般生物無法直接到達⋯⋯嘛，也許可以順著藤條爬上來，但難免爬到一半被幾十匹水色飛龍圍攻」

這就是它們生存的智慧吧
並不是誰教給它們，這些生物天生就擁有智慧
這樣想來，人類還真是愚蠢啊，平時總想著一些陳腐無聊的事情，真正重要的東西還必須特意考慮

「我們之前的計劃現在看來完全行不通呢⋯⋯還好有菲麗絲在。話說回來，菲麗絲沒問題吧？」

我向旁邊看去，在那裡的是村裡的姑娘，菲麗絲

她也跟著我們一起來了
當然，菲麗絲沒有戰鬥能力

那麼她為何要來呢，因為據她所說，穿過水色飛龍棲息地的方法，就是她自己