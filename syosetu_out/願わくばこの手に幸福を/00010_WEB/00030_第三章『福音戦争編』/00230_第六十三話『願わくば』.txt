赫爾特的一擊，不僅是聲音，甚至連空間都快割裂了。白刃在夜裡，以急速瞄準著我的臟腑。

為了回應那一擊。我右手握住的刀以最短的直線距離、最快的速度刺向他的頸部。
天才必須一次殺死，如果給了他第二次呼吸的機會，就注定是我的敗北了。必須憑這一擊取下他的頭顱，我保持著這樣的信念，揮舞著銀光。

同時，左手拿著的小刀放置到了赫爾特白刃的前進路線上。雖然我覺得防不住，但只要逼他選擇避開或者正面突破就行。我只要能避開最壊的結果就行。在那段時間裡，我會殺了他。

那正是一個呼吸之間的事。一呼氣，就預感到這場攻防就要結束了。那一手到底是來得及還是來不及？背後被汗水浸透。

──ガィンッ──キンッ

在瞬間的攻防中，我確實聽到了，兩種混雜起來的奇妙的聲音。
一個是，什麼越過了鋼鐵，宣告著自己絶命的聲音。左手握著的小刀，不要說迫使白刃更換軌道，就連稍加阻攔都無法實現就被擊斷了。

危險了。經過一系列攻防，刀刃被嚴重折磨，然後斷裂也沒什麼奇怪的。但是，現在就斷了？
不，不對。如果是赫爾特傾力而為的話，恐怕只要一擊就可以使刀折斷了，到現在為止的一系列攻防對他來說不過是開胃菜罷了。

與那聲音混雜在一起的是⋯使出全力揮舞小刀的左手握住的刀被白光彈飛的聲音，它折斷了。已經感覺不到它的存在了。但是，心中卻變得更加的熱切。

已經有預感了，毫無理由的預感。
由此，由我的手中向前突刺的刀刃是無法刺穿赫爾特的頸皮的。在那之前，我自己的身體就會被切成兩段，燃燒著的生命就此結束。

不好，如果無法使軌道發生一丁點的改變，我就會死。現在就是決命的時候了，要趕上，一定要趕上，千萬要趕上。

赫爾特的黃金瞳中散發出凶光。

───

變淡的夜幕中，卡麗婭神情恍惚地注視著雙方的攻防戰。

手裡握著銀色長劍。本來，在路易斯發生危機的時候，就該出手的。

但是已經沒必要了，對於卡麗婭來說，那已經不是重要的事了。
銀色的大眼睛晃動著，白色的臉頰為眼前的景象所感動，甚至附上了一抹朱紅。

路易斯的銀刃，赫爾特的白刃。兩雄の武器がまるで旋律を描くかのように互いに夜を切り裂く。決して路易斯の狀態は良しと言えない。むしろ、あっ、と聲を出す間にその首を刈り取られてしまいそうな氣配すらある。

だが、それでも、卡麗婭は胸の奧底からこみあげてくる喜びに近しい感情を押し殺すことが出來なかった。常に整っていたはずの表情は崩れ、今この時ばかりは感情のあるがままに委ねている。

あの男が、路易斯が紋章教徒の側に立ち、刃を振るっている。意志を胸に、施展著這種蠻勇。正是因為自己的引導。

天啊，還有什麼能比這更叫人歡喜了？卡麗婭腳後跟麻木地戰慄著。路易斯按照自己的意願，也就是在為自己而戰。那個叫芙拉朵的魔術師，一定也在某處見證這場戰鬥。但是。這場戰鬥不是為了你，而是為了我。

卡麗婭的心臟跳動不止。雖說事情進展得太快，直接就到了和赫爾特・斯坦利進行決鬥的地步了，說實話有點不妙。考慮最壊情況的話，需要考慮從背後切斷赫爾特的脖子了。

但是，已經不需要做那種事了。卡麗婭沒有揮劍的意願。那個男人，路易斯此時正燃燒生命進行著劍舞。

啊啊，想和赫爾特並列的話，那劍還太稚嫩了。即使步法很靈巧，也不能說是強者。覆蓋其身的就是凡庸，與那個天才為敵，我不認為他能活下去。儘管如此，即便如此。看到那個身姿，就能感受到他的覺悟。而踐踏那個的事，我怎麼能做？現在我能做的只有對那個身姿表示衷心的敬意。

路易斯和赫爾特已經進入到最後的攻防戰了。路易斯的左臂被折斷了，而右手的刀怎樣也觸及不到赫爾特的脖子。

卡麗婭把所有的決斷都寄托在心中。如果路易斯在這兒喪命，那個時候自己也必須與他為伴。那是將他帶到這個戰場的我的義務，是對路易斯最大的敬意和誠意。

──但是。衷心地祈願。──

那時。卡麗婭眯起了眼睛。儘管這並不想從這場攻防戰上移開視線。但此刻，陽光遮住了雙眼。

戰場即將迎來清晨。

───

背上感受到陽光的溫暖。盼望著的光芒，那燦爛的光輝。終於趕上了。

太陽切開了夜幕，重現威光。背對著陽光，我立於此。
黃金瞳，被陽光所閃耀。瞳孔反射性地縮小了。看見了，那原本應該斬斷身體的白刃，放緩了。雖然剖開了側腹，但還達不到臟腑的位置。

右手握著的銀刀發出光芒，必須於此處取下這位英雄的首級。如果無法一擊致死，這傢伙必將捲土重來。就在這裡，我們必須殺死他。

變得毫無間隙，刀刃步上刺進赫爾特脖子的軌道。

──在手上產生了剜肉的聲音和觸感。啊，可惡。

那是虛假的想像。我的刀，位於能在一瞬間到達的最短的距離。

因為他，赫爾特・斯坦利。強行驅動身體，使得刀尖所對的從頭變成了肩膀。刺進肩膀的小刀，因沾著他的血而閃閃發光。

就是這個，這令人難以置信的反應速度。肺部漏出氣息，這就是我和他之間的，凡人和天才間的差距嗎？看來我命該如此。
扭傷肩膀，赫爾特把小刀彈開。就這樣，朝著我的頭頂舉起劍。啊啊，我這邊已經無計可施了。

一手，還差一手。能夠填補平庸的我與天才赫爾特之間差距的還有什麼？胸口和身體都在發燙。雖然眼看就要死了，但臟腑仍在持續沸騰著。

「暫時的離別，路易斯──我的好敵手。」

赫爾特一邊說著這番話，一邊朝已經不能後退的我的頭蓋揮下白刃。陽光照耀著劍身。

──啊啊，拜託了，這雙手，需要一把劍。

───

芙拉朵那悲哀的嗚咽，隨風飄舞。

在拼死的攻防戰中，刀刃交接的那一刻，讓芙拉朵的胸口像被割裂一樣難受。又來了，又來了。他，又在勉強自己。路易斯又在竭盡全力，為了向無法觸及的東西伸出手來。明明放棄就好。明明逃開就行了。這樣就和自己沒關係了。平凡地，追求觸手可及的幸福的日子明明也是存在的。為什麼？

芙拉朵很清楚自己做不到這一點。她想起因無法觸及天才而日漸絶望的生活，咬緊牙關卻與求學本意漸行漸遠的每一天。

但路易斯卻無法接受那個事實，芙拉朵通過在地下神殿的那個場景就已經明白了。雖然和我一樣是平凡的人，但是即使冒著喪命的危險，也要伸出手的他的姿態。啊，那姿態是理想。正是我的理想。

正因如此，我，芙拉朵・娜・波魯克庫拉特才決心要將他變成黃金。而現在，路易斯卻要甩開這雙手了。

不要，討厭。我絶對不要承認這種事。
雖說如此，我也無法拿起武器來幫助路易斯。也無法用魔術進行支援。現在的這種場合，我什麼都做不到，無法幫助到他絲毫。

所以，眼眶被淚水濕潤，芙拉朵編織著語言。

那並不是什麼魔術。能和這座城市敵對的魔術，並不是現在的芙拉朵能施展出來的。所以，這只是普通的語言，只是祈禱的話語。也許沒有任何意義，但是，我堅信它一定會有效果。

毫無疑問。正是此身，使得他的身體被劍穿刺的罪魁禍首正是我。

──願你擁有幸福

在揮下劍的赫爾特面前，芙拉朵的言語使得空間發生了微微的震動。